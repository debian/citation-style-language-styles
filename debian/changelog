citation-style-language-styles (0.0.404-1) unstable; urgency=medium

  * Import upstream version 0.0.404.
  * Update years of packaging copyright.
  * debian/rules: move comments out of empty target.
  * Refresh debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Mar 2024 01:10:55 +0100

citation-style-language-styles (0.0.335-1) unstable; urgency=medium

  * Import upstream version 0.0.335.
  * debian/copyright.in: remove outdated comment about packaging
    Git snapshots.
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Dec 2023 21:44:27 +0100

citation-style-language-styles (0.0.288-1) unstable; urgency=medium

  * Import upstream version 0.0.288.
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 Aug 2023 20:21:10 +0200

citation-style-language-styles (0.0.287-1) unstable; urgency=medium

  * Use git tags for releases.
  * Import upstream version 0.0.287.
  * Refresh debian/copyright.
  * debian/rules: drop get-orig-source target as we are using
    git tags now.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Aug 2023 00:21:38 +0200

citation-style-language-styles (0~20230209.153790a-1) unstable; urgency=medium

  * Drop debian/gbp.conf.
    dgit stumbles over the removed .github directory.

  * Import upstream version 0~20230209.153790a.
  * Update years of packaging copyright,
    refresh upstream copyright notices.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Feb 2023 23:37:59 +0100

citation-style-language-styles (0~20220420.41ce2d4-1) unstable; urgency=medium

  * Import upstream version 0~20220420.41ce2d4.
  * Update years of packaging copyright.
  * Update upstream copyright notices.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Apr 2022 01:47:37 +0200

citation-style-language-styles (0~20210922.eaddf8e-1) unstable; urgency=medium

  * Import upstream version 0~20210922.eaddf8e.
  * Update debian/copyright for included files.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Sep 2021 18:32:30 +0200

citation-style-language-styles (0~20210819.04da306-1) unstable; urgency=medium

  * Import upstream version 0~20210819.04da306.
  * Update debian/copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Aug 2021 03:14:17 +0200

citation-style-language-styles (0~20210228.744de6d-1) unstable; urgency=medium

  * Update filters in debian/gbp.conf.
  * Update years of packaging copyright.
  * Import upstream version 0~20210228.744de6d.
  * Update upstream copyright notices.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Feb 2021 20:26:47 +0100

citation-style-language-styles (0~20201016.c5c3531-1) unstable; urgency=medium

  * Import upstream version 0~20201016.c5c3531.
  * Bump debhelper-compat to 13.
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Oct 2020 01:59:00 +0200

citation-style-language-styles (0~20200509.5dad23d-1) unstable; urgency=medium

  * Import upstream version 0~20200509.5dad23d.
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 May 2020 17:53:15 +0200

citation-style-language-styles (0~20200502.647fb56-1) unstable; urgency=medium

  * Update uscan's git mode in debian/watch.
  * Import upstream version 0~20200502.647fb56
  * Update years of packaging copyright.
  * Update upstream copyright notices.
  * Install upstream CONTRIBUTING file.
  * Update Vcs-* fields.
  * Drop lintian overrides about Vcs-* fields.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 12.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 18:25:20 +0200

citation-style-language-styles (0~20180122.283b8d871-1) unstable; urgency=medium

  * Initial release (closes: #841018).

 -- gregor herrmann <gregoa@debian.org>  Mon, 22 Jan 2018 18:57:06 +0100
